
library(Seurat)
library(data.table)
library(tidyverse)
library(scales)
library(ggrepel)
library(ggthemr)
library(ggpubr)
library(RColorBrewer)
library(DESeq2)
library(mygene)
library(XGR)
library(lme4)
library(sjstats)
library(lmerTest)
library(statmod)
library(UpSetR)
library(viridis)
library(ggnewscale)
library(rstatix)
library(circlize)
library(ComplexHeatmap)
library(survival)
library(survminer)
library(cowplot)
library(MASS)
library(org.Hs.eg.db)
library(networkD3)
library(htmlwidgets)

# Note - not all packages required for each figure:

# Generating the summary data for cross-over:

'%!in%' <- function(x,y)!('%in%'(x,y))
ggthemr(palette = "fresh")

colours_8 <- c("#111111", "navyblue", "#9C89B8", "#2A9D8F", "steelblue3", "#AED9E0", "grey85",  "darkorange2", "firebrick")

# Bulk RNAseq/MiXCR data
Bulk <- readRDS("C:/Users/Orion/Documents/Bulk_RNAseq/Bulk_use_v1.rds")
paired_samples <- (Bulk %>% dplyr::filter(cycle %in% c("C1", "C2")) %>% dplyr::select(ID, cycle) %>% distinct() %>% dplyr::count(ID) %>% dplyr::filter(n == 2))$ID
triple_samples <- (Bulk %>% dplyr::filter(cycle %in% c("C1", "C2", "C4")) %>% dplyr::select(ID, cycle) %>% distinct() %>% dplyr::count(ID) %>% dplyr::filter(n == 3))$ID
Samples <- Bulk %>% dplyr::filter(status %in% "patient")
Samples_C12 <- Samples %>% dplyr::filter(cycle %in% c("C1", "C2"))
Samples_C124 <- Samples %>% dplyr::filter(cycle %in% c("C1", "C2", "C4"))
Samples_Paired <- Samples_C12 %>% dplyr::filter(ID %in% paired_samples)
Samples_Tripled <- Samples_C124 %>% dplyr::filter(ID %in% triple_samples)

# Survival data
cox_proportional_data <- read.csv("C:/Users/Orion/Downloads/finalOxfordData_Jan21.csv")
cox <- cox_proportional_data %>%
  dplyr::mutate(Progression_status = ifelse(Progression_status=="YES",1,0), Death_status = ifelse(Death_status =="YES",1,0), AI_C5 = ifelse(AI_c5 =="YES",1,0) ) %>%
  dplyr::select(ID, age, Progression_status, Months_progression, Death_status, Months_death, AI_C5)

gm_mean = function(x, na.rm=TRUE){
  exp(sum(log(x[x > 0]), na.rm=na.rm) / length(x))
}

# Aesthetics
colours <- c("#111111", "black")
sc_palette <- define_palette(swatch=colours, gradient = c(lower = colours[1L], upper = colours[2L]))
ggthemr(sc_palette)


########################################################################################################

### ~~~~~ Fig 2b: Subset Score Changes

colours <- c("#111111", "steelblue3", "#AED9E0", "darkorange2", "firebrick", "navyblue")
sc_palette <- define_palette(swatch=colours, gradient = c(lower = colours[1L], upper = colours[2L]))
ggthemr(sc_palette)

Plot <- Samples_Paired %>% 
  dplyr::filter(tcrGene %in% "TRB", CDR3_type %in% "nSeqCDR3") %>%
  dplyr::filter(treatment_type %in% c("sICB", "cICB")) %>%
  dplyr::select(ID, cycle, CM, Effector, EM, MAIT, Mitotic, Naive) %>%
  distinct() %>%
  gather(-ID, -cycle, key = "Subset", value = "Score") %>%
  dplyr::mutate(Subset = factor(Subset, levels = c("Naive", "CM", "EM", "Effector", "Mitotic")))

Stat <- Plot %>%
  group_by(Subset) %>%
  wilcox_test(Score ~ cycle, paired = TRUE) %>%
  add_significance() %>%
  add_xy_position(x = "cycle") %>%
  dplyr::filter(!is.na(Subset))

Stat1 <- Stat %>% dplyr::filter(p < 0.05)
Stat2 <- Stat %>% dplyr::filter(Subset %!in% Stat1$Subset)

Plot %>%
  dplyr::filter(!is.na(Subset)) %>%
  ggplot(mapping = aes(x = cycle, y = Score)) +
  geom_point(data = Stat2, mapping = aes(x = "C1", y = y.position), alpha = 0) +
  ggbeeswarm::geom_quasirandom(mapping = aes(colour = Subset), groupOnX = TRUE, width = 0.3, show.legend = FALSE, alpha = 0.6, size = 1) +
  geom_boxplot(mapping = aes(fill = Subset, alpha = cycle), show.legend = FALSE, colour = "grey35", size = 0.35, outlier.size = 0, outlier.alpha = 0) +
  scale_alpha_manual(values = c(0.1, 0.3)) +
  stat_pvalue_manual(Stat1, size = 6, tip.length = 0.02) +
  scale_y_continuous(expand = expansion(mult = c(0.0, 0.075))) +
  theme(panel.grid = element_blank(), legend.position = "none") +
  facet_wrap(~Subset, scales = "free_y", nrow = 1) +
  xlab("timepoint") +
  ylab("Expression Score in Bulk")


### ~~~~~ Fig S2a: Bulk Score and Single Cell Prop Correlations

colours <- c("#111111", "steelblue3", "#AED9E0", "darkorange2", "firebrick", "navyblue")
sc_palette <- define_palette(swatch=colours, gradient = c(lower = colours[1L], upper = colours[2L]))
ggthemr(sc_palette)

x <- readRDS("C:/Users/Orion/Documents/Single_cell/New/Seurat_x_assigned.rds") # from the single-cell scripts
Combined <- x@meta.data %>%
  dplyr::filter(assignment_initial %in% c("Naive", "CM", "EM", "Effector", "Mitotic")) %>%
  group_by(donor, timepoint) %>% dplyr::count(assignment_initial) %>% dplyr::mutate(prop = prop.table(n)) %>% 
  ungroup() %>% complete(donor, timepoint, assignment_initial, fill = list(n = 0, prop = 0)) %>%
  dplyr::rename(c(sc_count = n, sc_prop = prop, Subset = assignment_initial, cycle = timepoint)) %>%
  ungroup() %>%
  dplyr::mutate(ID = case_when(donor %in% "1" ~ "80", 
                               donor %in% "2" ~ "81",
                               donor %in% "3" ~ "82",
                               donor %in% "4" ~ "83",
                               donor %in% "5" ~ "84",
                               donor %in% "6" ~ "85",
                               donor %in% "7" ~ "87",
                               donor %in% "8" ~ "88"
  )) %>%
  inner_join(
    Bulk %>% 
      dplyr::filter(tcrGene %in% "TRB", CDR3_type %in% "nSeqCDR3", cycle %in% c("C1", "C2")) %>%
      dplyr::select(ID, cycle, CM, Effector, EM, MAIT, Mitotic, Naive) %>%
      gather(-ID, -cycle, key = "Subset", value = "Score") %>%
      dplyr::mutate(ID = as.character(ID))
  ) %>%
  dplyr::mutate(Subset = factor(Subset, levels = c("Naive", "CM", "EM", "Effector", "Mitotic")))

Stat <- Combined %>%
  group_by(Subset) %>%
  cor_test(sc_prop, Score, method = "pearson") %>%
  dplyr::filter(Subset %in% c("Naive", "CM", "EM", "Effector", "Mitotic")) %>%
  dplyr::mutate(Subset = factor(Subset, levels = c("Naive", "CM", "EM", "Effector", "Mitotic")),
                p.val = p,
                p.adj = p.val * 5,
                p.val2 = case_when(p.val < 0.001 ~ formatC(p.val, format = "e", digits = 0), p.val > 1 ~ "1", TRUE ~ as.character(round(p.val, 3))),
                p.adj2 = case_when(p.adj < 0.001 ~ formatC(p.adj, format = "e", digits = 0), p.adj > 1 ~ "1", TRUE ~ as.character(round(p.adj, 3)))
  )

Combined %>%
  dplyr::filter(Subset %in% c("Naive", "CM", "EM", "Effector", "Mitotic")) %>%
  ggplot(mapping = aes(x = sc_prop, y = Score)) +
  geom_point(mapping = aes(colour = Subset), size = 2.5) +
  geom_smooth(method = "lm", mapping = aes(fill = Subset), colour = "black", linetype = "dotted", alpha = 0.15) +
  facet_wrap(~Subset, scales = "free", nrow = 1) +
  theme(legend.position = "none", panel.grid = element_blank()) +
  geom_text(data = Stat, mapping = aes(x = Inf, y = -Inf, label = paste("r=", round(cor, 2), "\np=", p.val2, sep = "")), hjust = 1, vjust = -0.5, size = 4) +
  # scale_fill_brewer(palette = "Set2") +
  # scale_colour_brewer(palette = "Set2") +
  xlab("Proportion by Single Cell") +
  ylab("Expression Score by Bulk")

### ~~~~~ Fig S2b: Bulk Score and Age Correlations

colours <- c("#111111", "steelblue3", "#AED9E0", "darkorange2", "firebrick", "navyblue")
sc_palette <- define_palette(swatch=colours, gradient = c(lower = colours[1L], upper = colours[2L]))
ggthemr(sc_palette)

Plot <- Bulk %>%
  dplyr::filter(tcrGene %in% "TRB", CDR3_type %in% "nSeqCDR3") %>%
  dplyr::filter(treatment_type %in% c("sICB", "cICB")) %>%
  dplyr::select(ID, cycle, age, CM, Effector, EM, MAIT, Mitotic, Naive) %>%
  gather(-ID, -cycle, -age, key = "Subset", value = "Score") %>%
  dplyr::mutate(Subset = factor(Subset, levels = c("Naive", "CM", "EM", "Effector", "Mitotic"))) %>%
  dplyr::filter(!is.na(Subset), !is.na(age))

Stat <- data.frame()
# i=1
for(i in 1:length(unique(Plot$Subset))){
  Sub <- unique(Plot$Subset)[i]
  res <- summary(lmer(Score ~ age + (1|cycle), data = Plot %>% dplyr::filter(Subset %in% Sub)))$coefficients["age", c("Estimate", "Pr(>|t|)")] %>%
    t() %>%
    as.data.frame() %>%
    `colnames<-`(c("Estimate", "pval")) %>%
    dplyr::mutate(Subset = Sub)
  Stat <- rbind(Stat, res)
}

Stat <- Stat %>%
  dplyr::mutate(p.val2 = case_when(pval < 0.001 ~ formatC(pval, format = "e", digits = 0), pval > 1 ~ "1", TRUE ~ as.character(round(pval, 3))))

Plot %>%
  ggplot(mapping = aes(x = age, y = Score)) +
  geom_point(colour = "grey85", alpha = 0.5) +
  geom_smooth(method = "lm", mapping = aes(fill = Subset), linetype = "dashed", colour = "black", show.legend = FALSE, alpha = 0.25) +
  geom_text(data = Stat, mapping = aes(x = Inf, y = -Inf, label = paste("p=", p.val2, sep = "")), hjust = 1, vjust = -1, size = 4) +
  theme(panel.grid = element_blank()) +
  facet_wrap(~Subset, scales = "free", nrow = 1) +
  xlab("Age (Years)") +
  ylab("Expression Score in Bulk")


### ~~~~~ Fig S2c: Mitotic Score across cycles

comparisons <- list(
  c(1, 2), c(1, 3)
)

Samples_Tripled %>%
  dplyr::filter(treatment_type %in% c("sICB", "cICB")) %>%
  dplyr::select(ID, cycle, Mitotic) %>%
  distinct() %>%
  ggplot(mapping = aes(x = cycle, y = Mitotic)) +
  ggbeeswarm::geom_quasirandom(mapping = aes(colour = cycle), groupOnX = TRUE, width = 0.15, alpha = 0.35, size = 3, show.legend = FALSE) +
  stat_compare_means(comparisons = comparisons, method = "wilcox", paired = FALSE, tip.length = 0.02, vjust = 0.5, label = "p.signif") +
  scale_fill_manual(name = "timepoint", values = c("#AED9E0", "steelblue3", "darkorange2")) +
  scale_colour_manual(name = "timepoint", values = c("#AED9E0", "steelblue3", "darkorange2")) +
  theme(panel.grid.major = element_line(colour = "grey85", size = 0.25), axis.title.x = element_blank()) +
  ylab("Mitotic Score")


### ~~~~~ Fig S2d: Correlations in subset score changes

Plot <- Samples_Paired %>%
  dplyr::filter(tcrGene %in% "TRB", CDR3_type %in% "nSeqCDR3") %>%
  dplyr::filter(treatment_type %in% c("sICB", "cICB")) %>%
  dplyr::select(ID, cycle, treatment_type, age, CM, Effector, EM, MAIT, Mitotic, Naive) %>%
  gather(-ID, -cycle, -treatment_type, -age, key = "Subset", value = "Score") %>%
  dplyr::filter(cycle %in% c("C1", "C2")) %>%
  spread(key = "cycle", value = "Score") %>%
  dplyr::mutate(Diff = C2 - C1) %>%
  dplyr::select(-C1, -C2) %>%
  dplyr::mutate(treatment_type = factor(treatment_type, levels = c("sICB", "cICB"))) %>%
  dplyr::filter(Subset %in% c("Naive", "CM", "EM", "Effector", "Mitotic"))

Subsets <- unique(Plot$Subset)
Stat <- data.frame()
# i=1
for(i in 1:length(Subsets)){
  Sub_1 <- Subsets[i]
  # j=1
  for(j in 1:length(Subsets)){
    Sub_2 <- Subsets[j]
    res <- cor.test(
      (Plot %>% dplyr::filter(Subset %in% Sub_1))$Diff,
      (Plot %>% dplyr::filter(Subset %in% Sub_2))$Diff,
      method = "pearson"
    )[1:4] %>%
      as.data.frame() %>%
      dplyr::mutate(Sub1 = Sub_1, Sub2 = Sub_2)
    Stat <- rbind(Stat, res)
  }
}

Plot <- Stat %>%
  dplyr::mutate(p.adj = p.value * 10, estimate = case_when(Sub1 == Sub2 ~ 0, Sub1 != Sub2 & p.adj < 0.05 ~ estimate, TRUE ~ 0),
                Sub1 = factor(Sub1, levels = c("Naive", "CM", "EM", "Effector", "Mitotic")),
                Sub2 = factor(Sub2, levels = rev(c("Naive", "CM", "EM", "Effector", "Mitotic")))
  )

Plot %>%
  ggplot(mapping = aes(x = Sub1, y = Sub2)) +
  geom_tile(mapping = aes(fill = estimate), linetype = "dashed", colour = "grey70", size = 0.3) +
  scale_fill_gradient2(low = "#9970AB", mid = "white", high = "#5AAE61", midpoint = 0, name = "rho") +
  scale_x_discrete(expand = c(0, 0)) +
  scale_y_discrete(expand = c(0, 0)) +
  geom_text(Plot %>% dplyr::filter(estimate != 0), mapping = aes(label = round(estimate, 2))) +
  theme(axis.title.y = element_blank()) +
  xlab("Change in Subset Score upon ICB")





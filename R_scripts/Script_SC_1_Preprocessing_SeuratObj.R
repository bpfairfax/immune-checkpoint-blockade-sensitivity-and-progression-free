# The outputs from QC are in outs/QC/Cells_passed_QC_noDoublet.txt as a list of names of all the cell barcodes that passed QC and read_count_passed_QC.txt as a matrix of the gene counts.

library(Seurat)
library(data.table)
library(tidyverse)

########################## STEP 1: INITIAL FILTERING AND WRANGLING ##########################

########## Generating count data and removing CD14 data ##########

Directory <- '/t1-data/user/otong/melanoma/Single_cell/inputs/'

samples <- as.list(c("A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8"))

Read <- function(x) {
  x = stringi::stri_reverse(x)
  x <- fread(paste(Directory, 'Expression/', x , '_read_count_passed_QC.txt', sep = ""), 
             stringsAsFactors = F)
  x = as.data.frame(x)
  rownames(x) <- as.character(x$V1)
  x <- x[,-1]
  x <- x[rowSums(x) > 0, ]
  x = x[,which(as.numeric(x[which(rownames(x) %in% c('CD14')),]) == 0)]
  }

counts <- lapply(samples, Read)

for(i in seq_along(counts)) {
  colnames(counts[[i]]) <- paste(samples[[i]], colnames(counts[[i]]), sep = "_")
}

Barcode <- function(x) {
  x = x[which(rownames(x) %in% c('CD8A', 'CD8B', 'CD3D')),]
  x[x > 0] = 1
  x = rbind(x, as.numeric(colSums(x[c(1,3),])))
  x = rbind(x, as.numeric(colSums(x[c(2,3),])))
  x[4,] = ifelse(x[4,] == 2, 2, 0)
  x[5,] = ifelse(x[5,] == 2, 2, 0)
  x = rbind(x, as.numeric(colSums(x[c(4,5),])))
  x = colnames(x)[which(x[6,] != 0)]
  }

names <- lapply(counts, Barcode)

for(i in seq_along(counts)) {
  counts[[i]] <- counts[[i]][, which(colnames(counts[[i]]) %in% names[[i]])]
  counts[[i]]$V1 <- rownames(counts[[i]])
}

all <- counts %>%
  purrr::reduce(full_join, by = "V1")

rownames(all) <- all$V1
all$V1 <- NULL

write.csv(all, paste(Directory, 'all.csv', sep = ""), row.names = TRUE)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~

########## Generating metadata ##########

NamesDF <- function(x) {
  x <- as.data.frame(x)
}

names <- lapply(names, NamesDF)

for(i in seq_along(names)) {
  names[[i]]$donor <- as.character(str_sub(samples[[i]], 2, 2)) 
  names[[i]]$cycle <- as.character(str_sub(samples[[i]], 1, 1))
  colnames(names[[i]]) <- c("barcode", "donor", "cycle")
  names[[i]]$sample <- i
}

all.names <- names %>% purrr::reduce(rbind)

all.names$cycle = as.factor(case_when(all.names$cycle %in% "A" ~ "1", all.names$cycle %in% "B" ~ "2"))

# table(colnames(all) %in% as.character(all.names$barcode))

write.csv(all.names, paste(Directory, 'all.names.csv', sep = ""), row.names = FALSE)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~

########## Generating VDJ data ##########

Directory <- '/t1-data/user/otong/melanoma/Single_cell/inputs/'

samples <- as.list(c("A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8"))

VDJRead <- function(x) {
  x = stringi::stri_reverse(x)
  x <- fread(paste(Directory, 'VDJ/', x , '_VDJ.csv', sep = ""), 
             stringsAsFactors = FALSE, header = TRUE)
}

VDJ <- lapply(samples, VDJRead)

for(i in seq_along(VDJ)) {
  VDJ[[i]]$barcode <- paste(samples[[i]], VDJ[[i]]$barcode, sep = "_") 
}

Exclude <- function(x) {
  x <- filter(x, cdr3 != "None")
  x <- filter(x, productive != "None")
  x <- filter(x, productive != "False")
  x <- filter(x, is_cell == TRUE)
  x <- filter(x, high_confidence == TRUE)
  x <- select(x, barcode, chain, v_gene, d_gene, j_gene, c_gene, cdr3, cdr3_nt, umis, raw_clonotype_id) }

VDJ <- lapply(VDJ, Exclude)

all.VDJ <- bind_rows(VDJ, .id = "barcode")

all.VDJ <- all.VDJ[, -1]

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~

########## Filtering VDJ data ##########

### TRA

all.TRAV <- filter(all.VDJ, chain %in% "TRA")
colnames(all.TRAV) <- c("barcode", paste("TRA", colnames(all.TRAV)[-1], sep = "_"))
all.TRAV.dup <- all.TRAV %>% group_by(barcode) %>% summarise(count = n()) %>% filter(count > 1)
all.TRAV$TRA <- case_when(all.TRAV$barcode %in% all.TRAV.dup$barcode ~ "Multiple", TRUE ~ "Unique")

all.TRAV.unique <- all.TRAV %>% filter(TRA %in% "Unique")
all.TRAV.dup <- all.TRAV %>% filter(TRA %in% "Multiple")
all.TRAV.dup <- as.data.frame(unique(all.TRAV.dup$barcode))
all.TRAV.dup$TRA <- "Multiple"
colnames(all.TRAV.dup) <- c("barcode", "TRA")

all.TRAV.unique <- plyr::rbind.fill(all.TRAV.unique, all.TRAV.dup)

all.TRAV.unique %>% filter(TRA %in% "Multiple")

length(all.TRAV.unique$barcode)
length(all.TRAV$barcode)

### TRB

all.TRBV <- filter(all.VDJ, chain %in% "TRB")
colnames(all.TRBV) <- c("barcode", paste("TRB", colnames(all.TRBV)[-1], sep = "_"))
all.TRBV.dup <- all.TRBV %>% group_by(barcode) %>% summarise(count = n()) %>% filter(count > 1)
all.TRBV$TRB <- case_when(all.TRBV$barcode %in% all.TRBV.dup$barcode ~ "Multiple", TRUE ~ "Unique")

all.TRBV.unique <- all.TRBV %>% filter(TRB %in% "Unique")
all.TRBV.dup <- all.TRBV %>% filter(TRB %in% "Multiple")
all.TRBV.dup <- as.data.frame(unique(all.TRBV.dup$barcode))
all.TRBV.dup$TRB <- "Multiple"
colnames(all.TRBV.dup) <- c("barcode", "TRB")

all.TRBV.unique <- plyr::rbind.fill(all.TRBV.unique, all.TRBV.dup)

all.TRBV.unique %>% filter(TRB %in% "Multiple")

length(all.TRBV.unique$barcode)
length(all.TRBV$barcode)

### Combining TRA and TRB

all.other <- filter(all.VDJ, chain != "TRA" & chain != "TRB") # checked here for TRG and TRD chains, but none seen?

all.TRAB <- full_join(all.TRAV.unique, all.TRBV.unique, by = "barcode")
all.TRAB <- replace_na(all.TRAB, list(TRA = "Unknown", TRB = "Unknown"))

all.TRAB <- all.TRAB %>% mutate(MAIT = case_when(TRA_v_gene %in% "TRAV1-2" & TRA_j_gene %in% c("TRAJ12", "TRAJ20", "TRAJ33") ~ "MAIT", TRA %in% c("Multiple", "Unknown") ~ "Unknown", TRUE ~ "non-MAIT"))

write.csv(all.TRAB, paste(Directory, 'all.TRAB.csv', sep = ""), row.names = FALSE)

saveRDS(all.TRAB, file = paste(Directory, 'all.TRAB.rds', sep = ""))

# length(unique(all.TRAB$barcode))
# length(all.TRAB$barcode)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~

########## Combining metadata and VDJ data ##########

### Loading all the required objects into R

Directory <- '/t1-data/user/otong/melanoma/Single_cell/inputs/'

all.names <- fread(paste(Directory, 'all.names.csv', sep = ""), stringsAsFactors = F)
all.names = as.data.frame(all.names)

all.TRAB <- fread(paste(Directory, 'all.TRAB.csv', sep = ""), stringsAsFactors = F)
all.TRAB = as.data.frame(all.TRAB)

# table(all.TRAB$barcode %in% all.names$barcode)

all.meta <- left_join(all.names, all.TRAB, by = "barcode")
all.meta <- replace_na(all.meta, list(TRA_umis = 0, TRB_umis = 0))
all.meta[is.na(all.meta)] <- as.character("None")
all.meta$TCR <- case_when(all.meta$TRA %in% "None" & all.meta$TRB %in% "None" ~ "No", TRUE ~ "Yes")

write.csv(all.meta, paste(Directory, 'all.meta.csv', sep = ""), row.names = FALSE)

saveRDS(all.meta, file = paste(Directory, 'all.meta.rds', sep = ""))

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

########################## STEP 2: RUNNING SEURAT ##########################

########## Modifications based on user design ##########

library(Seurat)
library(data.table)
library(tidyverse)

Directory <- '/t1-data/user/otong/melanoma/Single_cell/inputs/'

all.meta <- fread(paste(Directory, 'all.meta.csv', sep = ""), stringsAsFactors = F)
rownames(all.meta) <- all.meta$barcode

########## Loading in count data and matching it to metadata ##########

all <- fread(paste(Directory, 'all.csv', sep = ""), stringsAsFactors = F)
all = as.data.frame(all)
rownames(all) <- all$V1
all$V1 <- NULL
all <- all %>% select(all.meta$barcode)

# dim(all)
# dim(all.meta)
# table(colnames(all) %in% rownames(all.meta))

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~

########## Creating the Seurat object ##########

x_preQC <- CreateSeuratObject(all, meta.data = all.meta, min.cells = 5)

x_preQC[["percent.mt"]] <- PercentageFeatureSet(x_preQC, pattern = "^MT-")

saveRDS(x_preQC, file = '/t1-data/user/otong/melanoma/Single_cell/Seurat_x_Pass_1_preQC.rds')

##### Check QC parameters of the Seurat object in home R environment ######

x_preQC <- readRDS("Seurat_x_Pass_1_preQC.rds")

# check whether the standard parameters of nCount_RNA >= 500, nFeature_RNA >= 300 and percent.mt < 20 look okay

########## Initial QC of the Seurat object ##########

x <- subset(x = x_preQC, subset = (nCount_RNA >= 500) &
                                       (nFeature_RNA >= 300) &
                                       (percent.mt < 20))

saveRDS(x, file = '/t1-data/user/otong/melanoma/Single_cell/Seurat_x_postQC.rds')

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~

load('/t1-data/user/otong/melanoma/Single_cell/cycle.rda')
x <- NormalizeData(object = x)
x <- CellCycleScoring(object = x, g2m.features = g2m_genes, s.features = s_genes)
x <- FindVariableFeatures(object = x, selection.method = "vst", nfeatures = 2000)
x <- ScaleData(object = x, vars.to.regress = c("percent.mt ", "G2M.Score", "S.Score"))
x <- RunPCA(object = x, npcs = 50)

saveRDS(x, '/t1-data/user/otong/melanoma/Single_cell/Seurat_x_Pass.rds')

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~

########## APPROACH TO PERFORMING CLUSTERING + ASSIGNMENT ##########

# 1. Use a quantitative Elbow Plot to determine the number of principal components to use for clustering
# 2. Using this value, deliberately over-cluster the data using a high value for FindClusters, using Clustree to determine a rough cutoff
# 3. Generate UMAP clusters using this value + initial visualisations
# 4. Use SingleR to agnostically assign identities to each cluster


########## 1: QUANTITATIVE ELBOW PLOT ##########

# Determine the number of relevant PCs using a quantitative elbow plot 
# according to https://hbctraining.github.io/scRNA-seq/lessons/elbow_plot_metric.html

x <- readRDS("C:/Users/Orion/Documents/Single_cell/New/Seurat_x_Pass.rds")

pct <- x[["pca"]]@stdev / sum(x[["pca"]]@stdev) * 100
cumu <- cumsum(pct)
co1 <- which(cumu > 90 & pct < 5)[1]
co2 <- sort(which((pct[1:length(pct) - 1] - pct[2:length(pct)]) > 0.1), decreasing = T)[1] + 1
pcs <- min(co1, co2)
plot_df <- data.frame(pct = pct, 
                      cumu = cumu, 
                      rank = 1:length(pct))
ggplot(plot_df, aes(cumu, pct, label = rank, color = rank > pcs)) + 
  geom_text() + 
  theme_bw()

# Based on this determination, use 16 pcs
# use.pcs = 1:pcs

use.pcs = 1:16

########## 2: LOUVAIN CLUSTERING AND UMAP ##########

# First step in workflow is to FindNeighbors using the predetermined no. of pcs
# Second step is to FindClusters using a high value for the resolution argument, giving us a deliberate over-clustering
# Typically, I go with 1.5 but you can do this more quantitatively by using Clustree (in the OPTIONAL section)
# Third stpe is to RunUMAP using the predetermined no. of pcs

# x <- FindNeighbors(x, reduction = "pca", dims = use.pcs)
# x <- FindClusters(x, resolution = 1.5)
# x <- RunUMAP(object = x, reduction = "pca", dims = use.pcs)

### Using Clustree

# We can use a range of resolution values for FindClusters, and then plot how many clusters we will end up getting, and also how they branch off at each stage of clustering:

x <- FindNeighbors(x, reduction = "pca", dims = use.pcs)
x <- FindClusters(x, resolution = c(0, 0.1, 0.5, 1.0, 1.5, 2.0, 2.5))
clustree(x, prefix = "RNA_snn_res.", edge_width = 1.3) +
  scale_colour_hue(h = c(0, 270), name = "FindClusters\nResolution") +
  scale_edge_color_viridis(option = "viridis") +
  guides(
    edge_colour = FALSE,
    size = FALSE,
    edge_alpha = FALSE,
    colour = guide_legend(override.aes = list(size = 5))
  )

# Based on the plot that is generated, resolution of 1.5 gives quite good stability and in addition not many new clusters are formed between 1.5 and 2.0

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Clustering approach:

x <- readRDS("C:/Users/Orion/Documents/Single_cell/New/Seurat_x_Pass.rds")
use.pcs = 1:16
x <- FindNeighbors(x, reduction = "pca", dims = use.pcs)
x <- FindClusters(x, resolution = 1.5)
x <- RunUMAP(object = x, reduction = "pca", dims = use.pcs)

saveRDS(x, "C:/Users/Orion/Documents/Single_cell/New/Seurat_x_clustered.rds")

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~

########## 4: SINGLE_R ASSIGNMENT ##########

########## SINGLER PT.1: SUBSET DETERMINATION ##########

### CD8 T cell subsets:

# Naive, CM, EM, Effector, TEMRA, MAIT are ones that can be cleanly delineated by flow and are of general interest

### References (subsets + key markers)

# Peripheral T cell expansion predicts tumour infiltration and clinical response. Nature. 2020. Wu et al.
# https://www.nature.com/articles/s41586-020-2056-8#Sec1

# 8.1-Teff: GZMH, GNLY, NKG7, KLRD1
# 8.2-Tem: GZMK, DUSP2, CLDND1
# 8.3-Trm1/2/3: ZNF683, ITGAE, CD69, XCL1, KLRC2, KLRC3, XCL2
# 8.4-Chromatin regulation (activated?): 
# 8.5-Mitosis (exhausted): TUBA1B, HMGN2, HMGB1, TUBB
# 8.6-KLRB1 (MAIT)

# Global characterization of T cells in non-small-cell lung cancer by single-cell sequencing. Nature Medicine. 2018. Guo et al.
# https://www.nature.com/articles/s41591-018-0045-3/figures/1

# CD8-C1-LEF1 naive CD8: TCF7, SELL, LEF1, CCR7
# CD8-C2-CD28: CD28 (higher), LYAR, SESN1, EPB41, CYP5B1
# CD8-C3-CX3CR1 effector CD8: TBX21, ZEB2, GNLY
# CD8-C4-GZMK: GZMK, EOMES
# CD8-C5-ZNF683 Trm CD8: ZNF683
# CD8-C6-LAYN exhausted CD8: LAG3, HAVCR2, IFNG, TOX
# CD8-C7-SLC4A10 MAIT

# Lineage tracking reveals dynamic relationships of T cells in colorectal cancer. Nature. 2018. Zhang et al. 
# https://www.nature.com/articles/s41586-018-0694-x

# CD8-C1-LEF1 Tnaive: CCR7-high, LEF1-high, SELL-high, TCF7-high, ACTN1, PRKCQ-AS1
# CD8-C2-GPR183 TCM: IL7R, CD28, GPR183
# CD8-C3-CX3CR1 TEMRA/TEFF: 
# CD8-C4-GZMK TEM:
# CD8-C5-CD6 TRM:
# CD8-C6-CD160 IEL:
# CD8-C7-LAYN Tex:
# CD8-C8-SLC4A10 MAIT:

# Clonal replacement of tumor-specific T cells following PD-1 blockade. Nature Medicine. 2019. Yost et al.
# https://www.nature.com/articles/s41591-019-0522-3/figures/6

# CD8 naive: CCR7
# CD8 memory: EOMES, GZMK, CXCR3
# CD8 effector memory: FGFBP2, KLRD1
# CD8 exhausted: GZMB, ENTPD1, ITGAE  
# CD8 activated: TNF, IFNG, FOS, JUN
# CD8 activated/exhausted: LAG3, HAVCR2, PDCD1

# Single-cell profiling of breast cancer T cells reveals a tissue-resident memory subset associated with improved prognosis. Nature Medicine. 2018. Savas et al.
# https://www.nature.com/articles/s41591-018-0078-7

# CD8 Trm: HAVCR2, GZMB, CCL3, CCL4, VCAM1, KLRC1
# CD8 Trm mitotic: HMGB2, TUBA1B, TUBB, H2AFZ, CKS1B, STMN1, CCNA2
# CD8 Tem: KLRG1, LYAR, GZMK, TXNIP, FCRL6
# CD8 gd: TRGC2, ZNF683, XCL1, XCL2, IFITM3

# Across all the datasets, we can see a general pattern of shared cell types:
# Tnaive: TCF7, SELL, LEF1, CCR7
# Tcm: CD28, SESN1, GPR183, IL7R-high
# Tem: GZMK, EOMES, DUSP2, CLDND1
# Teff/Temra: GZMH, GNLY, NKG7, KLRD1
# Trm/exhausted: ZNF683, ITGAE, KLRC2, KLRC3, XCL1, XCL2
# Tex/mitosis: TUBA1B, HMGN2, HMGB1, TUBB
# MAIT: KLRB1, ZBTB16, SLC4A10, TRAV1-2

# The datasets that best contain all of these are the ones from Guo et al. and Zhang et al. but we can still use all of the references to generate as much information as possible

########## SINGLER PT.2: PREPARATION OF DATASETS ##########

# Data should ideally all in the same format for inference purposes
# We can generate logTPM counts for each of the datasets and then feed these into SingleR in the next part (codes adapted from Wu et al)
# For each of the references, we need two things:
# 1. TPM matrix of genes x cells
# 2. Vector of reference identities for cells (in the exact same order)

########## 1. SC DATA ##########

x <- readRDS('/t1-data/user/otong/melanoma/Single_cell/Seurat_x_Pass.rds')
raw.counts <- as.matrix(GetAssayData(x, slot = "counts"))
raw.counts <- raw.counts[which(rowSums(raw.counts) > 0),] 
lib.sizes <- colSums(raw.counts)
cpm.t <- t(raw.counts) / lib.sizes # Transpose prior to division
cpm.t <- 1e6 * cpm.t + 1    # Add pseudocount
log2cpm.t <- log2(cpm.t)
log2cpm <- t(log2cpm.t)
saveRDS(log2cpm, '/t1-data/user/otong/melanoma/Single_cell/SingleR/Pass_1B_lognormTPMcounts.rds')


########## 2. GUO ET AL. ##########

# Data downloaded from https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE99254
# Metadata: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?view=data&acc=GSE99254&id=7744&db=GeoDb_blob160
# Raw counts: "GSE99254_NSCLC.TCell.S12346.count.txt.gz"

# Metadata > Vector of cell identities
Guo.meta <- fread('/t1-data/user/otong/melanoma/Single_cell/Published_references/Guo_2018_NatMed_metadata.txt', stringsAsFactors = F, header = F)
colnames(Guo.meta) <- as.character(Guo.meta[1,])
Guo.meta <- Guo.meta[-1,]
clustersCD8 <- unique(Guo.meta$majorCluster)[12:18]
Guo.meta.CD8 <- Guo.meta %>% filter(majorCluster %in% clustersCD8)
Guo.CD8 <- as.character(Guo.meta.CD8$majorCluster)
saveRDS(Guo.CD8, '/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Guo_CD8_subsetlist.rds')

# Counts > TPM matrix
Guo <- fread('/t1-data/user/otong/melanoma/Single_cell/Published_references/Guo_2018_NatMed_rawcounts.txt', stringsAsFactors = F)
Guo <- as.data.frame(Guo)
# table(is.na(Guo$symbol))
nas <- which(is.na(Guo[,2]))
Guo <- Guo[-nas, ]
genes <- Guo$symbol
Guo <- as.matrix(Guo %>% select(Guo.meta.CD8$UniqueCell_ID))
rownames(Guo) <- genes

lib.sizes <- colSums(Guo)
Guo.t <- t(Guo) / lib.sizes # Transpose prior to division
Guo.t <- 1e6 * Guo.t + 1    # Add pseudocount
log2Guo.t <- log2(Guo.t)
log2Guo <- t(log2Guo.t)

saveRDS(log2Guo, '/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Guo_CD8_lognormTPMcounts.rds') 

########## 3. ZHANG ET AL. ##########

# Data downloaded from https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE108989
# Metadata: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?view=data&acc=GSE108989&id=166188&db=GeoDb_blob163
# Raw counts: "GSE108989_CRC.TCell.S11138.count.txt.gz"

# Metadata > Vector of cell identities
Zhang.meta <- fread('/t1-data/user/otong/melanoma/Single_cell/Published_references/Zhang_2018_Nature_metadata.txt', stringsAsFactors = F, header = F)
colnames(Zhang.meta) <- as.character(Zhang.meta[1,])
Zhang.meta <- Zhang.meta[-1,]
clustersCD8 <- unique(Zhang.meta$majorCluster)[13:20]
Zhang.meta.CD8 <- Zhang.meta %>% filter(majorCluster %in% clustersCD8)
Zhang.CD8 <- as.character(Zhang.meta.CD8$majorCluster)
saveRDS(Zhang.CD8, '/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Zhang_CD8_subsetlist.rds')

# Counts > TPM matrix
Zhang <- fread('/t1-data/user/otong/melanoma/Single_cell/Published_references/Zhang_2018_Nature_rawcounts.txt', stringsAsFactors = F)
Zhang <- as.data.frame(Zhang)
# table(is.na(Zhang$symbol))
nas <- which(is.na(Zhang[,2]))
Zhang <- Zhang[-nas, ]
genes <- Zhang$symbol
Zhang <- as.matrix(Zhang %>% select(Zhang.meta.CD8$UniqueCell_ID))
rownames(Zhang) <- genes

lib.sizes <- colSums(Zhang)
Zhang.t <- t(Zhang) / lib.sizes # Transpose prior to division
Zhang.t <- 1e6 * Zhang.t + 1    # Add pseudocount
log2Zhang.t <- log2(Zhang.t)
log2Zhang <- t(log2Zhang.t)
saveRDS(log2Zhang, '/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Zhang_CD8_lognormTPMcounts.rds') 


########## 4. YOST ET AL. ##########

# Data downloaded from https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE123813
# Metadata: "GSE123813_bcc_tcell_metadata.txt.gz"
# Raw counts: "GSE123813_bcc_scRNA_counts.txt.gz"

# Metadata > Vector of cell identities

Yost.meta <- fread('/t1-data/user/otong/melanoma/Single_cell/Published_references/Yost_2019_NatMed_metadata_CD8only.txt', stringsAsFactors = F, header = T)
clustersCD8 <- unique(Yost.meta$cluster)[c(1, 4, 6, 7, 8, 9)]
Yost.meta.CD8 <- Yost.meta %>% dplyr::filter(cluster %in% clustersCD8)

Yost <- fread('/t1-data/user/otong/melanoma/Single_cell/Published_references/Yost_2019_NatMed_rawcounts.txt', stringsAsFactors = F)
Yost <- as.data.frame(Yost)

YostCD3 <- Yost[ , unique(c(which(Yost[which(Yost$V1 %in% "CD3D"),] > 0), which(Yost[which(Yost$V1 %in% "CD3E"),] > 0), which(Yost[which(Yost$V1 %in% "CD3G"),] > 0)))]
YostCD8 <- YostCD3[ , unique(c(which(YostCD3[which(YostCD3$V1 %in% "CD8A"),] > 0), which(YostCD3[which(YostCD3$V1 %in% "CD8B"),] > 0)))]

genes <- Yost$V1
Yost$V1 <- NULL

CD8names <- intersect(colnames(YostCD8), Yost.meta.CD8$cell.id)

Yost.use.CD8 <- Yost.meta.CD8 %>% dplyr::filter(cell.id %in% CD8names)
rownames(Yost.use.CD8) <- Yost.use.CD8$cell.id

Yost <- as.matrix(Yost %>% select(Yost.use.CD8$cell.id))
rownames(Yost) <- genes

Yost.names <- (Yost.use.CD8 %>% dplyr::filter(cell.id %in% colnames(Yost)))$cluster
saveRDS(Yost.names, '/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Yost_CD8_subsetlist.rds')

lib.sizes <- colSums(Yost)
Yost.t <- t(Yost) / lib.sizes # Transpose prior to division
Yost.t <- 1e6 * Yost.t + 1    # Add pseudocount
log2Yost.t <- log2(Yost.t)
log2Yost <- t(log2Yost.t)
saveRDS(log2Yost, '/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Yost_CD8_lognormTPMcounts.rds')

########## 5. WU ET AL. ##########

# PB Only

LB6 <- Read10X('/t1-data/user/otong/melanoma/Single_cell/Published_references/Wu/GSM4143667_SAM24363331-lb6/')
RB1 <- Read10X('/t1-data/user/otong/melanoma/Single_cell/Published_references/Wu/GSM4143680_SAM24360639-rb1/')
RB2 <- Read10X('/t1-data/user/otong/melanoma/Single_cell/Published_references/Wu/GSM4143683_SAM24361564-rb2/')
RB3 <- Read10X('/t1-data/user/otong/melanoma/Single_cell/Published_references/Wu/GSM4143686_SAM24363038-rb3/')

List <- list(
  LB6,
  RB1,
  RB2,
  RB3
)

Seurat <- vector(mode = "list", length = length(List))

for(i in seq_along(List)) {
  Seurat[[i]] <- CreateSeuratObject(counts = List[[i]], min.cells = 3)
}

Seurat <- merge(Seurat[[1]], y = c(Seurat[[2]], Seurat[[3]], Seurat[[4]]), add.cell.ids = c("LB6", "RB1", "RB2", "RB3"))

Seurat@meta.data$barcode <- rownames(Seurat@meta.data)

Wu.meta <- fread('/t1-data/user/otong/melanoma/Single_cell/Published_references/Wu_2020_Nature_metadata.txt', stringsAsFactors = F, header = T)
Wu.meta$barcode <- Wu.meta$V1
clusters <- unique(Wu.meta$ident)[c(1, 4, 5, 6, 13, 14, 15, 16)]
Wu.meta <- Wu.meta %>% filter(ident %in% clusters)

Seurat <- subset(Seurat, barcode %in% Wu.meta$barcode)

Seurat@meta.data$rownames <- rownames(Seurat@meta.data)
Seurat@meta.data <- Seurat@meta.data %>% left_join(Wu.meta)
rownames(Seurat@meta.data) <- Seurat@meta.data$rownames

x <- Seurat
Wu.use <- x@meta.data$ident
saveRDS(Wu.use, '/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Wu_CD8_subsetlist.rds')


raw.counts <- as.matrix(GetAssayData(x, slot = "counts"))
raw.counts <- raw.counts[which(rowSums(raw.counts) > 0),]  # 13915 x 10000
lib.sizes <- colSums(raw.counts)
cpm.t <- t(raw.counts) / lib.sizes # Transpose prior to division
cpm.t <- 1e6 * cpm.t + 1    # Add pseudocount
log2cpm.t <- log2(cpm.t)
log2Wu <- t(log2cpm.t)
saveRDS(log2Wu, '/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Wu_CD8_lognormTPMcounts.rds')


########## 6. SAVAS ET AL. ##########

# Data downloaded from https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE110686
# NOTE: no metadata files were ever uploaded, so cell identities had to be INFERRED based on self-clustering and matching expression values/cluster locations
# Raw counts: "GSE110686_RAW.tar"

# Creating a Seurat object to identify cells belonging to their described clusters
x <- Read10X('/t1-data/user/otong/melanoma/Single_cell/Published_references/Savas/')
x <- CreateSeuratObject(counts = x, min.cells = 3, min.features = 200)
x[["percent.mt"]] <- PercentageFeatureSet(x, pattern = "^MT-")
x <- subset(x, subset = nCount_RNA < 7500 & nFeature_RNA < 2500 & percent.mt < 4)
x <- NormalizeData(object = x)
x <- FindVariableFeatures(object = x, selection.method = "vst", nfeatures = 2000)
x <- ScaleData(object = x, vars.to.regress = c("percent.mt", "nCount_RNA"))
x <- RunPCA(object = x, npcs = 50)
pct <- x[["pca"]]@stdev / sum(x[["pca"]]@stdev) * 100
cumu <- cumsum(pct)
co1 <- which(cumu > 90 & pct < 5)[1]
co2 <- sort(which((pct[1:length(pct) - 1] - pct[2:length(pct)]) > 0.1), decreasing = T)[1] + 1
pcs <- min(co1, co2)
plot_df <- data.frame(pct = pct, 
                      cumu = cumu, 
                      rank = 1:length(pct))
ggplot(plot_df, aes(cumu, pct, label = rank, color = rank > pcs)) + 
  geom_text() + 
  theme_bw()
use.pcs = 1:13
x <- FindNeighbors(x, reduction = "pca", dims = use.pcs)
x <- FindClusters(x, resolution = 1.0)
x <- RunUMAP(object = x, reduction = "pca", dims = use.pcs)
x <- subset(x, CD8A > 0)
# DimPlot(x, label = TRUE)
# FeaturePlot(x, features = "CD8A", label = TRUE) # 11, 3, 12, 2, 9
# FeaturePlot(x, features = c("CD8A", "TUBA1B", "HMGN2", "TUBB"), label = TRUE) # 11
# FeaturePlot(x, features = c("HAVCR2", "GZMB", "VCAM1"), label = TRUE) # 3, 12
# FeaturePlot(x, features = c("KLRG1", "LYAR", "GZMK"), label = TRUE) # 2
# FeaturePlot(x, features = c("TRGC2", "XCL1", "XCL2", "ZNF683"), label = TRUE) # 9
meta <- x@meta.data
meta$barcode <- rownames(meta)
meta <- meta %>% filter(seurat_clusters %in% c("11", "3", "12", "2", "9"))
meta$asgn.cluster <- case_when(meta$seurat_clusters %in% "11" ~ "Trm.mitotic",
                               meta$seurat_clusters %in% "2" ~ "Tem",
                               meta$seurat_clusters %in% "9" ~ "Tgd",
                               TRUE ~ "Trm")
saveRDS(meta, "/t1-data/user/otong/melanoma/Single_cell/Published_references/Savas_2018_NatMed_metadata.rds")
raw.counts <- as.data.frame(GetAssayData(x, slot = "counts"))
saveRDS(raw.counts, "/t1-data/user/otong/melanoma/Single_cell/Published_references/Savas_2018_NatMed_rawcounts.rds")

# Metadata > Vector of cell identities
Savas.all <- meta$asgn.cluster
saveRDS(Savas.all, '/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Savas_CD8_subsetlist.rds')

# Counts > TPM matrix
genes <- rownames(raw.counts)
Savas <- as.matrix(raw.counts %>% select(meta$barcode))
rownames(Savas) <- genes

lib.sizes <- colSums(Savas)
Savas.t <- t(Savas) / lib.sizes # Transpose prior to division
Savas.t <- 1e6 * Savas.t + 1    # Add pseudocount
log2Savas.t <- log2(Savas.t)
log2Savas <- t(log2Savas.t)
saveRDS(log2Savas, '/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Savas_CD8_lognormTPMcounts.rds')


########## SINGLER PT.3: RUNNING SINGLER ##########

log2cpm <- readRDS('/t1-data/user/otong/melanoma/Single_cell/SingleR/Pass_1B_lognormTPMcounts.rds')
log2Guo <- readRDS('/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Guo_CD8_lognormTPMcounts.rds')
log2Zhang <- readRDS('/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Zhang_CD8_lognormTPMcounts.rds')
log2Yost <- readRDS('/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Yost_CD8_lognormTPMcounts.rds')
log2Wu <- readRDS('/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Wu_CD8_lognormTPMcounts.rds')
log2Savas <- readRDS('/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Savas_CD8_lognormTPMcounts.rds')

Guo.use <- readRDS('/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Guo_CD8_subsetlist.rds')
Zhang.use <- readRDS('/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Zhang_CD8_subsetlist.rds')
Yost.use <- readRDS('/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Yost_CD8_subsetlist.rds')
Wu.use <- readRDS('/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Wu_CD8_subsetlist.rds')
Savas.use <- readRDS('/t1-data/user/otong/melanoma/Single_cell/SingleR/ref_Savas_CD8_subsetlist.rds')

# The actual command to run SingleR is quite straightforward and is just the function SingleR, specifying test, ref and labels

# We want to run SingleR using these datasets: Guo, Zhang, Yost, Wu, Savas

# 2. Running SingleR

library(SingleR)

results <- SingleR(test = log2cpm, ref = log2Guo, labels = Guo.use, de.method = "wilcox")
results.x <- as.data.frame(cbind(rownames(results), results$labels)) %>% dplyr::filter(!is.na(V2))
colnames(results.x) <- c("barcode", "assignment")
results.x$barcode <- as.character(results.x$barcode)
results.x$assignment <- as.character(results.x$assignment)
saveRDS(results.x, '/t1-data/user/otong/melanoma/Single_cell/SingleR/SingleR_x_Guo.rds')

results <- SingleR(test = log2cpm, ref = log2Zhang, labels = Zhang.use, de.method = "wilcox")
results.x <- as.data.frame(cbind(rownames(results), results$labels)) %>% dplyr::filter(!is.na(V2))
colnames(results.x) <- c("barcode", "assignment")
results.x$barcode <- as.character(results.x$barcode)
results.x$assignment <- as.character(results.x$assignment)
saveRDS(results.x, '/t1-data/user/otong/melanoma/Single_cell/SingleR/SingleR_x_Zhang.rds')

results <- SingleR(test = log2cpm, ref = log2Yost, labels = Yost.use, de.method = "wilcox")
results.x <- as.data.frame(cbind(rownames(results), results$labels)) %>% dplyr::filter(!is.na(V2))
colnames(results.x) <- c("barcode", "assignment")
results.x$barcode <- as.character(results.x$barcode)
results.x$assignment <- as.character(results.x$assignment)
saveRDS(results.x, '/t1-data/user/otong/melanoma/Single_cell/SingleR/SingleR_x_Yost.rds')

results <- SingleR(test = log2cpm, ref = log2Wu, labels = Wu.use, de.method = "wilcox")
results.x <- as.data.frame(cbind(rownames(results), results$labels)) %>% dplyr::filter(!is.na(V2))
colnames(results.x) <- c("barcode", "assignment")
results.x$barcode <- as.character(results.x$barcode)
results.x$assignment <- as.character(results.x$assignment)
saveRDS(results.x, '/t1-data/user/otong/melanoma/Single_cell/SingleR/SingleR_x_Wu.rds')

results <- SingleR(test = log2cpm, ref = log2Savas, labels = Savas.use, de.method = "wilcox")
results.x <- as.data.frame(cbind(rownames(results), results$labels)) %>% dplyr::filter(!is.na(V2))
colnames(results.x) <- c("barcode", "assignment")
results.x$barcode <- as.character(results.x$barcode)
results.x$assignment <- as.character(results.x$assignment)
saveRDS(results.x, '/t1-data/user/otong/melanoma/Single_cell/SingleR/SingleR_x_Savas.rds')

########## 5: SUMMARY OF THE SINGLER ASSIGNMENTS ##########

# Based on the inferred identity for each Seurat cluster:

#  0: Naive
#  1: MAIT
#  2: CM
#  3: Effector
#  4: CM (slightly difficult to distinguish from Naive)
#  5: Effector
#  6: EM
#  7: EM
#  8: Naive
#  9: Effector
# 10: Effector
# 11: Effector
# 12: Effector
# 13: Effector
# 14: EM
# 15: EM
# 16: Naive
# 17: CM
# 18: Naive
# 19: MAIT
# 20: MAIT
# 21: Mitosis-ex
# 22: Mitosis-ex
# 23: Unknown
# 24: Unknown/MAIT

# Naive: 0, 8, 16, 18
# CM: 2, 4, 17
# EM: 6, 7, 14, 15
# Effector: 3, 5, 9, 10, 12, 13
# Effector Trm-ex: 11
# MAIT: 1, 19, 20
# Mitosis-ex: 21, 22
# Unknown: 23, 24

########## 6: GAMMA DELTA ##########

TRDC <- colnames(x@assays$RNA@data)[which(x@assays$RNA@data["TRDC",] > 0)]
x@meta.data$TRDC <- case_when(x@meta.data$barcode %in% TRDC ~ "TRDC", TRUE ~ "No")

########## 7: FINAL ASSIGNMENTS ##########

x@meta.data$assignment_initial <- case_when(
  x@meta.data$TRDC %in% "TRDC" ~ "gd",
  x@meta.data$TRDC %in% "No" & x@meta.data$seurat_clusters %in% c("0", "8", "16", "18") ~ "Naive",
  x@meta.data$TRDC %in% "No" & x@meta.data$seurat_clusters %in% c("2", "4", "17") ~ "CM",
  x@meta.data$TRDC %in% "No" & x@meta.data$seurat_clusters %in% c("6", "7", "14", "15") ~ "EM",
  x@meta.data$TRDC %in% "No" & x@meta.data$seurat_clusters %in% c("1", "19", "20") ~ "MAIT",
  x@meta.data$TRDC %in% "No" & x@meta.data$seurat_clusters %in% c("21", "22") ~ "Mitotic",
  x@meta.data$TRDC %in% "No" & x@meta.data$seurat_clusters %in% c("23", "24") ~ "Unknown",
  TRUE ~ "Effector"
)

saveRDS(x, "C:/Users/Orion/Documents/Single_cell/New/Seurat_x_assigned.rds")



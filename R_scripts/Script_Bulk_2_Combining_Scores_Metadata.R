##########################################################
# Final script for figures:
##########################################################

library(data.table)
library(tidyverse)
library(mygene)

'%!in%' <- function(x,y)!('%in%'(x,y))

# --- Loading in files needed to create scores:

CD8_exp <- readRDS("C:/Users/Orion/Documents/Bulk_RNAseq/cd8_data_all.rds")
CD8_meta <- readRDS("C:/Users/Orion/Documents/Bulk_RNAseq/cd8_meta2.rds")
CD8_meta <- CD8_meta[-(2:8)] %>% distinct()

# --- Useful items:

# Adding ENSG names:

ENSG <- rownames(CD8_exp)
Symbols <- as.data.frame(getGenes(ENSG, fields = "symbol"))
# saveRDS(Symbols, "C:/Users/Orion/Documents/Single_cell/New/Symbol.rds")
# Symbols <- as.data.frame(readRDS("C:/Users/Orion/Documents/Single_cell/New/Symbol.rds"))
Symbols <- Symbols %>% dplyr::mutate(gene = symbol) %>% dplyr::filter(is.na(notfound)) %>% dplyr::select(gene, query)

gm_mean = function(x, na.rm=TRUE){
  exp(sum(log(x[x > 0]), na.rm=na.rm) / length(x))
}


##########################################################
# Setting up marker lists:
##########################################################

# # x <- readRDS("C:/Users/Orion/Documents/Single_cell/New/Seurat_x_assigned.rds")
# 
# # Idents(x) <- "assignment_initial"
# # C1 <- FindAllMarkers(subset(x, assignment_initial %!in% "Unknown" & cycle %in% "1"))
# # C2 <- FindAllMarkers(subset(x, assignment_initial %!in% "Unknown" & cycle %in% "2"))
# # 
# # saveRDS(C1, "C:/Users/Orion/Documents/Single_cell/New/C1_markers.rds")
# # saveRDS(C2, "C:/Users/Orion/Documents/Single_cell/New/C2_markers.rds")
# 
# C1 <- readRDS("C:/Users/Orion/Documents/Single_cell/New/C1_markers.rds")
# C2 <- readRDS("C:/Users/Orion/Documents/Single_cell/New/C2_markers.rds")
# 
# Subset <- as.list(c("Naive", "CM", "EM", "Effector", "gd", "Mitotic", "MAIT"))
# KeyMarkers <- vector(mode = "list", length = length(Subset))
# 
# for(i in 1:length(Subset)){
#   KeyMarkers[[i]] <- paste(Subset[[i]], intersect(
#     (C1 %>% filter(avg_logFC > 0, p_val_adj < 0.01, cluster %in% Subset[[i]]) %>% arrange(p_val_adj))$gene[1:20],
#     (C2 %>% filter(avg_logFC > 0, p_val_adj < 0.01, cluster %in% Subset[[i]]) %>% arrange(p_val_adj))$gene[1:20]
#   ), sep = "_")
# }
# 
# KeyMarkers <- unlist(KeyMarkers)
# 
# List <- list(C1, C2)
# 
# for(i in seq_along(List)) {
#   List[[i]][ , paste("p_adj", i, sep = "")] <- List[[i]]$p_val_adj
#   List[[i]][ , paste("avg_logFC", i, sep = "")] <- List[[i]]$avg_logFC
#   List[[i]] <- List[[i]][ , c(6:9)] %>% dplyr::mutate(sub_gene = paste(cluster, gene, sep = "_")) %>% dplyr::filter(sub_gene %in% KeyMarkers)
# }
# 
# Markers <- full_join(List[[1]], List[[2]]) %>% dplyr::select(-sub_gene)
# Markers <- Markers %>% dplyr::filter(str_sub(gene, 1, 3) %!in% "HLA"
#                                      , gene %!in% Markers$gene[which(duplicated(Markers$gene))]
# ) %>% 
#   dplyr::mutate(avg_FC = (avg_logFC1 + avg_logFC2)/ 2)
# 
# Markers %>% dplyr::count(cluster)
# 
# saveRDS(Markers, "C:/Users/Orion/Documents/Single_cell/New/Markers_bulkRNAseq_subsets.rds")
Markers <- readRDS("C:/Users/Orion/Documents/Single_cell/New/Markers_bulkRNAseq_subsets.rds")


# dupgenes <- (Symbols %>% dplyr::count(gene) %>% dplyr::filter(n > 1))$gene
# table(Markers$gene %in% dupgenes)

(Markers <- Markers %>% left_join(Symbols)) 


##########################################################
# Generating scores using marker lists
##########################################################

Subset <- as.list(c("Naive", "CM", "EM", "Effector", "gd", "Mitotic", "MAIT"))
Markers.List <- vector(mode = "list", length = length(Subset))
Data.List <- vector(mode = "list", length = length(Subset))

for(i in 1:length(Subset)) {
  Markers.List[[i]] <- (Markers %>% dplyr::filter(cluster %in% Subset[[i]]))$query
}

for(i in seq_along(Markers.List)) {
  Data.List[[i]] <- as.data.frame(t(CD8_exp[which(rownames(CD8_exp) %in% Markers.List[[i]]), ]))
  Data.List[[i]]$Subset <- Subset[[i]]
  Data.List[[i]] <- Data.List[[i]] %>% rownames_to_column("id") %>% gather(-id, -Subset, key = "gene", value = "Expression") %>%
    # left_join(Markers %>% dplyr::mutate(gene = query, Subset = cluster) %>% dplyr::select(avg_FC, gene, Subset)) %>%
    # dplyr::mutate(Expression = Expression * avg_FC) %>%
    group_by(id) %>% summarise(Expression = gm_mean(Expression))
  Data.List[[i]]$Subset <- Subset[[i]]
}

sub_scores <- as.data.frame(Data.List %>% purrr::reduce(rbind)) %>% spread(key = "Subset", value = "Expression")


##########################################################
# Adding in scoring data:
##########################################################

Genes <- readRDS("C:/Users/Orion/Documents/Single_cell/New/Genes_list.rds")

Activation <- unlist(Genes[[1]])[1:50]
Cytotoxicity <- unlist(Genes[[2]])[1:50]
Exhaustion <- unlist(Genes[[3]])[1:50]
Ex_2 <- unlist(Genes[[7]])[1:50]
 
Markers_List <- list(Activation, Cytotoxicity, Exhaustion, Ex_2)
names <- c("Activation", "Cytotoxicity", "Exhaustion", "Ex_2")

gene_scores <- data.frame()
# i=1
for(i in 1:length(Markers_List)) {
  MarkersUse <- Symbols$query[match(Markers_List[[i]], Symbols$gene)]
  Bulk <- as.data.frame(t(CD8_exp[which(rownames(CD8_exp) %in% MarkersUse), ])) %>% rownames_to_column(var = "id") %>%
    gather(-id, key = "Gene", value = "Expression") %>% group_by(id) %>% summarise(Expression = gm_mean(Expression))
  Bulk$score <- names[i]
  gene_scores <- rbind(gene_scores, Bulk)
}

gene_scores <- gene_scores %>% spread(key = "score", value = "Expression")


##########################################################
# Adding in clonal data
##########################################################

exp_clones <- readRDS("C:/Users/Orion/Documents/Bulk_RNAseq/mixcr_results_3_combined.rds")


##########################################################
# Adding in survival/patient data:
##########################################################

metadata <- as.data.frame(fread("C:/Users/Orion/Documents/Bulk_RNAseq/BLOOD_META_v1.3.csv"))
path <- as.data.frame(fread("C:/Users/Orion/Documents/Bulk_RNAseq/BLOOD_PATH_v1.3.csv"))
cox_proportional_data <- read.csv("C:/Users/Orion/Downloads/finalOxfordData_Jan21.csv")

cox <- cox_proportional_data %>%
  dplyr::mutate(Progression_status = ifelse(Progression_status=="YES",1,0), Death_status = ifelse(Death_status =="YES",1,0), AI_C5 = ifelse(AI_c5 =="YES",1,0) ) %>%
  dplyr::select(ID, age, Progression_status, Months_progression, Death_status, Months_death, AI_C5)

meta <- metadata %>%
  dplyr::filter(!is.na(ID)) %>%
  dplyr::mutate(NPB = ifelse(ID == "103", "NPB_03757", NPB)) %>% # incorrectly enterred
  dplyr::mutate(Rx = ifelse(Rx %in% "Nivo", "nivo", Rx),
                treatment_type = ifelse(Rx %in% "ipiNivo", "cICB", ifelse(Rx %in% c("AdjNivo", "AdjPembro"), "adj_sICB", "sICB"))) %>%
  dplyr::mutate(date_progression = lubridate::dmy(date_progression), 
                progression_last_checked_date = lubridate::dmy(progression_last_checked_date), 
                date_death = lubridate::dmy(date_death), 
                death_last_checked_date = lubridate::dmy(death_last_checked_date), 
                Rx_start_date = lubridate::dmy(Rx_start_date))

meta$overall_progression <- case_when(
  meta$ID %in% "25" ~ "1",
  !is.na(meta$date_progression) == TRUE ~ "1",
  is.na(meta$date_progression) == TRUE & !is.na(meta$progression_last_checked_date) == TRUE ~ "0",
  is.na(meta$date_progression) == TRUE & is.na(meta$progression_last_checked_date) == TRUE ~ "NA"
)

meta$progression_months <- case_when(
  meta$ID %in% "25" ~ 0,
  meta$overall_progression %in% "1" ~ as.double(difftime(meta$date_progression, meta$Rx_start_date, units = "days")) / 30,
  meta$overall_progression %in% "0" ~ as.double(difftime(meta$progression_last_checked_date, meta$Rx_start_date, units = "days")) / 30,
  meta$overall_progression %in% "NA" ~ 0
)

meta$death_months <- case_when(
  meta$death_status %in% "1" ~ as.double(difftime(meta$date_death, meta$Rx_start_date, units = "days")) / 30,
  meta$death_status %in% "0" ~ as.double(difftime(meta$death_last_checked_date, meta$Rx_start_date, units = "days")) / 30
)

meta$progression_at_6months <- case_when(
  meta$ID %in% "25" ~ "NA",
  meta$ID %in% "137" ~ "0",
  meta$overall_progression %in% "1" & meta$progression_months < 7 ~ "1",
  meta$overall_progression %in% "0" & meta$progression_months > 6 ~ "0",
  meta$overall_progression %in% "1" & meta$progression_months > 7 ~ "0",
  TRUE ~ "NA"
) # P35, 55, 63, 99 and 151 are all borderline

meta$progression_at_12months <- case_when(
  meta$ID %in% "25" ~ "NA",
  meta$ID %in% "137" ~ "NA",
  meta$overall_progression %in% "1" & meta$progression_months < 13 ~ "1",
  meta$overall_progression %in% "0" & meta$progression_months > 12 ~ "0",
  meta$overall_progression %in% "1" & meta$progression_months > 13 ~ "0",
  TRUE ~ "NA"
) # P35, 55, 63, 99 and 151 are all borderline

path$subtype <- case_when(path$subtype %in% c("E", "M", "NOS", "U") ~ path$subtype, 
                                          path$subtype %in% c("uveal") ~ "U",
                                          path$subtype %in% c("small cell") ~ "SC",
                                          path$subtype %in% c("amelanocytic") ~ "A"
)

meta_small <- meta %>% dplyr::select(ID, age, overall_progression, progression, death_status, months_death, months_progression, progression_months,
                                     AI_c5, progression_at_6months, progression_at_12months, progression_6months, death_months)

##########################################################
# Combining everything:
##########################################################

Bulk <- CD8_meta %>% dplyr::select(id, ID, status, cycle, NPB) %>%
  left_join(meta %>% dplyr::select(
    ID, NPB, age, sex, Rx, cancer, treatment_type, progression_6months, progression_months, progression_at_6months, progression_at_12months, overall_progression, progression_months, death_months, AI_c5, death_status
    )) %>% # by ID, NPB, age, sex 
  left_join(path %>% dplyr::select(ID, subtype)) %>%
  left_join(sub_scores) %>% # by id
  left_join(gene_scores) %>% # by id
  left_join(exp_clones) # by id, ID, cycle (expanding into 4: chain x aa/nucleotide calling)
  
paired_samples <- (Bulk %>% dplyr::filter(cycle %in% c("C1", "C2")) %>% dplyr::select(ID, cycle) %>% distinct() %>% dplyr::count(ID) %>% dplyr::filter(n == 2))$ID
Samples <- Bulk %>% dplyr::filter(status %in% "patient")
Samples_C12 <- Samples %>% dplyr::filter(cycle %in% c("C1", "C2"))
Samples_Paired <- Samples_C12 %>% dplyr::filter(ID %in% paired_samples)

##########################################################
# Adding in C1-C2 score changes:
##########################################################

score_change <- Samples_Paired %>% 
  dplyr::select(ID, cycle, Activation, Cytotoxicity, Ex_2, Exhaustion, CM, Effector, EM, gd, MAIT, Mitotic, Naive) %>% 
  distinct() %>%
  gather(-ID, -cycle, key = "score", value = "expression") %>%
  spread(key = "cycle", value = "expression") %>%
  dplyr::mutate(change = C2 - C1) %>%
  dplyr::select(-C1, -C2) %>%
  spread(key = "score", value = "change")

colnames(score_change) <- c("ID", paste(colnames(score_change)[-1], "Change", sep = "_"))  

Bulk <- Bulk %>% left_join(score_change)

saveRDS(Bulk, "C:/Users/Orion/Documents/Bulk_RNAseq/Bulk_use_v1.rds")


### END
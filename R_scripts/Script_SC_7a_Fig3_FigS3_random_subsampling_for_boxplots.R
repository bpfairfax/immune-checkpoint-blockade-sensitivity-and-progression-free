# This script aims subsample ECs and EMs and do a DEG analysis on the subsample, repeated 100 times for each size category
# It is computationally demanding and so it is advised to parrellise on a HPC cluster. 
# R Watson


#Sections
# 1. Subsample and do DEG for each size category
# 2. Subsample and do DEG for big v small ECs (same script for EMs)
# 3. Subsample and do DEG for leave-one-out analysis

#Libraries
library(Seurat)
library(tidyverse)
library(limma)
library(ggrepel)
library(XGR)

'%notin%'<-Negate('%in%')

#Load files and set up the object


#==============================================================================================================
#==============================================================================================================
# 1. Subsample and do DEG for each size category
#==============================================================================================================
#==============================================================================================================

#This generates the boxplot in fig 3d

#Load files and set up the object
x<- readRDS('~seurat_cells_with_vdj.rds')

#create list of cells from metadata
cells<-x@meta.data[,c('barcode','cycle','clone_ID','assignment_final','category_num_pre','clone_proportion','treatment','orig.ident')]
cells$category_num_pre<- ifelse(cells$category_num_pre=='1', paste('<0.25%'),
                                ifelse(cells$category_num_pre=='2', paste('0.25-0.5%'),
                                       ifelse(cells$category_num_pre=='3', paste('0.5-2.5%'), paste('>2.5%'))))
cells$category_num_pre<-factor(cells$category_num_pre, levels=c('<0.25%','0.25-0.5%','0.5-2.5%','>2.5%'))


Cells_pre<-cells %>% filter(cycle==1)
Cells_post<-cells %>% filter(cycle==2)

clones_both<-Cells_pre[which(Cells_pre$clone_ID %in% Cells_post$clone_ID),"clone_ID"]
clones_both<-clones_both[!duplicated(clones_both)]



categories<-c('<0.25%','0.25-0.5%','0.5-2.5%','>2.5%')
Cells_both_pre_list<-list()
for(i in 1:length(categories)){
  Cells_both_pre_list[[i]]<- cells %>% filter(clone_ID %in% clones_both) %>% 
    filter(cycle==1, category_num_pre==categories[i])
}
Cells_both_post_list<-list()
for(i in 1:length(categories)){
  Cells_both_post_list[[i]]<- cells %>% filter(clone_ID %in% clones_both) %>% 
    filter(cycle==2, category_num_pre==categories[i])
}

names<-categories
Cells.pre.rand<-lapply(names, function(x){vector('list',100)})
Cells.post.rand<-lapply(names, function(x){vector('list',100)})

temp<-c(Cells_both_pre_list, Cells_both_post_list)

for(i in 1:length(categories)){
  for(j in 1:100){
    Cells.pre.rand[[i]][[j]]<-sample(Cells_both_pre_list[[i]]$barcode,min(sapply(temp, nrow)))
  }
}

for(i in 1:length(categories)){
  for(j in 1:100){
    Cells.post.rand[[i]][[j]]<-sample(Cells_both_post_list[[i]]$barcode,min(sapply(temp, nrow)))
  }
}


Cells_4_cats_summary<-lapply(categories, function(x){vector('list',100)})

for(i in 1:length(categories)){
  for(j in 1:100){
    x <- readRDS("~seurat_cells_with_vdj.rds")
    x<-SetIdent(x, cells=Cells.post.rand[[i]][[j]], value='post')
    x<-SetIdent(x, cells=Cells.pre.rand[[i]][[j]], value='pre')
    temp<-FindMarkers(x,ident.1 = 'post', ident.2 = 'pre',logfc.threshold = -Inf, min.pct = -Inf)
    temp$gene<-row.names(temp)
    temp$comparison<-categories[i]
    temp<-temp[which(temp$p_val_adj<0.01),]
    Cells_4_cats_summary[[i]][[j]]<-length(temp$gene)
  }
}

cat_num.DEG.summary<-Cells_4_cats_summary

for(i in 1:4){
  cat_num.DEG.summary[[i]]<-as.numeric(cat_num.DEG.summary[[i]])
}

summarydf<-data.frame()
for(i in 1:4){
  temp<-tibble(cat_num.DEG.summary[[i]], i)
  summarydf<-rbind(summarydf, temp)
}


summarydf<-as.data.frame(summarydf)
summarydf[,2]<-as.numeric(summarydf[,2])
colnames(summarydf)<-c('number','Category')

summarydf$Category<- ifelse(summarydf$Category=='1', paste('<0.25%'),
                            ifelse(summarydf$Category=='2', paste('0.25-0.5%'),
                                   ifelse(summarydf$Category=='3', paste('0.5-2.5%'), paste('>2.5%'))))
summarydf$Category<-factor(summarydf$Category, levels=c('<0.25%','0.25-0.5%','0.5-2.5%','>2.5%'))


ggplot(summarydf, aes(Category, number)) + 
  geom_boxplot(aes(fill = Category), outlier.shape = NA) +
  ylab('Number of DEGs')+
  xlab('Clone size as proportion of repertoire')+
  theme(legend.position = 'none')+
  stat_compare_means(comparisons = my_comparisons,  paired = FALSE, label='p.signif')


#==============================================================================================================
#==============================================================================================================
# 2. Subsample and do DEG for big v small ECs (same script for EMs)
#==============================================================================================================
#==============================================================================================================

#This generates the boxplots in fig 3e. The example given here is for big v small ECs. The same script can be used
# for EMs, simply change the subsetting from 'Effector' to 'EM'. Again, this is computationally demanding and a HPC is advisable

#Load files and set up the object
x<- readRDS('~seurat_cells_with_vdj.rds')

cells<-x@meta.data[,c('barcode','cycle','clone_ID','assignment_final','category_num_pre','clone_proportion','treatment', 'orig.ident')]

cells$category_num_pre<- ifelse(cells$category_num_pre=='1', paste('<0.25%'),
                                ifelse(cells$category_num_pre=='2', paste('0.25-0.5%'),
                                       ifelse(cells$category_num_pre=='3', paste('0.5-2.5%'), paste('>2.5%'))))
cells$category_num_pre<-factor(cells$category_num_pre, levels=c('<0.25%','0.25-0.5%','0.5-2.5%','>2.5%'))

ECs<-cells %>% filter(assignment_final=='Effector')
ECs$binary_baseline_size<-ifelse(ECs$category_num_pre %in% c('0.5-2.5%','>2.5%'), paste('large'), paste('small'))
ECs_pre<-cells %>% filter(assignment_final=='Effector', cycle==1)
ECs_post<-cells %>% filter(assignment_final=='Effector', cycle==2)

clones_both<-ECs_pre[which(ECs_pre$clone_ID %in% ECs_post$clone_ID),"clone_ID"]
clones_both<-clones_both[!duplicated(clones_both)]


categories<-c('large', 'small')
ECs_both_pre_list<-list()
for(i in 1:length(categories)){
  ECs_both_pre_list[[i]]<- ECs %>% filter(clone_ID %in% clones_both) %>% 
    filter(cycle==1, binary_baseline_size==categories[i])
}
ECs_both_post_list<-list()
for(i in 1:length(categories)){
  ECs_both_post_list[[i]]<- ECs %>% filter(clone_ID %in% clones_both) %>% 
    filter(cycle==2, binary_baseline_size==categories[i])
}

names<-categories
EC_binary.pre.rand<-lapply(names, function(x){vector('list',100)})
EC_binary.post.rand<-lapply(names, function(x){vector('list',100)})

temp<-c(ECs_both_pre_list, ECs_both_post_list)
min(sapply(temp, nrow))

for(i in 1:length(categories)){
  for(j in 1:100){
    EC_binary.pre.rand[[i]][[j]]<-sample(ECs_both_pre_list[[i]]$barcode,min(sapply(temp, nrow)))
  }
}

for(i in 1:length(categories)){
  for(j in 1:100){
    EC_binary.post.rand[[i]][[j]]<-sample(ECs_both_post_list[[i]]$barcode,min(sapply(temp, nrow)))
  }
}


EC_binary.eff.DEG.summary<-lapply(categories, function(x){vector('list',100)})


for(i in 1:length(categories)){
  for(j in 1:100){
x <- readRDS("~seurat_cells_with_vdj.rds")
x<-SetIdent(x, cells=EC_binary.post.rand[[i]][[j]], value='post')
x<-SetIdent(x, cells=EC_binary.pre.rand[[i]][[j]], value='pre')
temp<-FindMarkers(x,ident.1 = 'post', ident.2 = 'pre',logfc.threshold = -Inf, min.pct = -Inf)
temp$gene<-row.names(temp)
temp$comparison<-categories[i]
temp<-temp[which(temp$p_val_adj<0.01),]
EC_binary.eff.DEG.summary[[i]][[j]]<-length(temp$gene)
  }
}


EC_summ<-EC_binary.eff.DEG.summary

summary_df_EC<-data.frame()
for(i in 1:length(EC_summ)){
  temp<-data.frame(category=rep(categories[i], length(EC_summ[[i]])), num_genes=EC_summ[[i]], cell=rep('EC',length(EC_summ[[i]])))
  summary_df_EC<-rbind(summary_df_EC, temp)
}

summary_df_EC$category<-ifelse(summary_df_EC$category=='large', paste('>0.5%'), paste('<0.5%'))
summary_df_EC$category<-factor(summary_df_EC$category, levels = c('>0.5%', '<0.5%'))
summary_df_EC$cell<-factor(summary_df_EC$cell, levels=c('EC','EM'))
mycomparison<-list(c('>0.5%', '<0.5%'))


ggplot(summary_df_EC, aes(category, num_genes)) +
  geom_point()+
  geom_boxplot(mapping = aes(fill = cell), outlier.size = 0, show.legend = FALSE, alpha = 0.9, colour='grey45') +
  stat_compare_means(comparisons=mycomparison,paired=TRUE,tip.length = 0.02, vjust=0.1,  label = 'p.signif')+
  labs(y='Number of DEG pre- and post-ICB', x='Clone size as proportion of repertoire')


#==============================================================================================================
#==============================================================================================================
# 3. Subsample and do DEG for leave-one-out analysis
#==============================================================================================================
#==============================================================================================================

#first we create random lists of cells for comparsion, but missing one individual each time
individuals<-c(1:8)


for(a in 1:length(individuals)){
individual= individuals[a]


x<- readRDS('~seurat_cells_with_vdj.rds')
x<-subset(x, !(orig.ident==paste0('A',individual) | orig.ident==paste0('B',individual)))

#create list of cells from metadata
cells<-x@meta.data[,c('barcode','cycle','clone_ID','assignment_final','category_num_pre','clone_proportion','treatment', 'orig.ident')]

cells$category_num_pre<- ifelse(cells$category_num_pre=='1', paste('<0.25%'),
                                ifelse(cells$category_num_pre=='2', paste('0.25-0.5%'),
                                       ifelse(cells$category_num_pre=='3', paste('0.5-2.5%'), paste('>2.5%'))))
cells$category_num_pre<-factor(cells$category_num_pre, levels=c('<0.25%','0.25-0.5%','0.5-2.5%','>2.5%'))

ECs<-cells %>% filter(assignment_final=='Effector')
ECs$binary_baseline_size<-ifelse(ECs$category_num_pre %in% c('0.5-2.5%','>2.5%'), paste('large'), paste('small'))
ECs_pre<-cells %>% filter(assignment_final=='Effector', cycle==1)
ECs_post<-cells %>% filter(assignment_final=='Effector', cycle==2)

clones_both<-ECs_pre[which(ECs_pre$clone_ID %in% ECs_post$clone_ID),"clone_ID"]
clones_both<-clones_both[!duplicated(clones_both)]

ECs$binary_baseline_size<-ifelse(ECs$category_num_pre %in% c('0.5-2.5%','>2.5%'), paste('large'), paste('small'))


categories<-c('large', 'small')
ECs_both_pre_list<-list()
for(i in 1:length(categories)){
  ECs_both_pre_list[[i]]<- ECs %>% filter(clone_ID %in% clones_both) %>% 
    filter(cycle==1, binary_baseline_size==categories[i])
}
ECs_both_post_list<-list()
for(i in 1:length(categories)){
  ECs_both_post_list[[i]]<- ECs %>% filter(clone_ID %in% clones_both) %>% 
    filter(cycle==2, binary_baseline_size==categories[i])
}

names<-categories
EC_binary.pre.rand<-lapply(names, function(x){vector('list',100)})
EC_binary.post.rand<-lapply(names, function(x){vector('list',100)})

temp<-c(ECs_both_pre_list, ECs_both_post_list)

for(i in 1:length(categories)){
  for(j in 1:100){
    EC_binary.pre.rand[[i]][[j]]<-sample(ECs_both_pre_list[[i]]$barcode,min(sapply(temp, nrow)))
  }
}

for(i in 1:length(categories)){
  for(j in 1:100){
    EC_binary.post.rand[[i]][[j]]<-sample(ECs_both_post_list[[i]]$barcode,min(sapply(temp, nrow)))
  }

  }



  EC_binary.eff.DEG.summary<-lapply(categories, function(x){vector('list',100)})


  for(i in 1:length(categories)){
    for(j in 1:100){

x <- readRDS('~seurat_cells_with_vdj.rds')
  x<-subset(x, !(orig.ident==paste0('A',individual) | orig.ident==paste0('B',individual)))
  x<-SetIdent(x, cells=EC_binary.post.rand[[i]][[j]], value='post')
  x<-SetIdent(x, cells=EC_binary.pre.rand[[i]][[j]], value='pre')
  temp<-FindMarkers(x,ident.1 = 'post', ident.2 = 'pre',logfc.threshold = -Inf, min.pct = -Inf)
  temp$gene<-row.names(temp)
  temp$comparison<-categories[i]
  temp<-temp[which(temp$p_val_adj<0.01),]
  EC_binary.eff.DEG.summary[[i]][[j]]<-length(temp$gene)

  saveRDS(EC_binary.eff.DEG.full, paste0('~/EC_LOO_random_DEGs_indiv_', individual, '.rds'))
  saveRDS(EC_binary.eff.DEG.summary, paste0('~/EC_LOO_random_DEGs_indiv_', individual, '.rds'))
    }
  }    
}

summary_df_list<-list()  
for(a in 1:length(individuals)){
  individual = individuals[a]
    summary<-readRDS(paste0('~/EC_LOO_summary_indiv_', individual, '.rds'))
    summary_df<-data.frame()

for(i in 1:length(summary)){
  temp<-data.frame(category=rep(categories[i], length(summary[[i]])), num_genes=summary[[i]], donor=rep(individual,length(summary[[i]])))
  summary_df<-rbind(summary_df, temp)
}
   
  summary_df_list[[a]]<-summary_df

}


summary_df<-plyr::ldply(summary_df_list, rbind)

summary_df$category<-ifelse(summary_df$category=='large', paste('>0.5%'), paste('<0.5%'))
summary_df$category<-factor(summary_df$category, levels = c('>0.5%', '<0.5%'))
summary_df$donor<-as.character(summary_df$donor)
summary_df$missing<-paste('Missing individual', summary_df$donor, sep=' ')
mycomparison<-list(c('>0.5%', '<0.5%'))

ggplot(summary_df, aes(category, num_genes)) +
  geom_point()+
  geom_boxplot(fill='firebrick', outlier.size = 0, show.legend = FALSE, alpha = 0.9, colour='grey45') +
  facet_wrap(vars(missing), scales='free_y', nrow=2)+
  stat_compare_means(comparisons=mycomparison,paired=TRUE,tip.length = 0.02, vjust=0.1,  label = 'p.signif')+
  labs(y='Number of DEG pre- and post-ICB', x='Clone size as proportion of repertoire')


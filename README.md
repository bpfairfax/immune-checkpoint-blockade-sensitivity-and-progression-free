# README #

This repository contains scripts for the analysis and generation of figures found in the publication "Immune Checkpoint Blockade sensitivity and Progression-Free Survival associates with baseline CD8+ T cell clone size and cytotoxicity", DOI: https://doi.org/10.1101/2020.11.15.383786 


## Associated data ##

Raw sequencing data (fastq files) has been deposited at the European Genome–phenome Archive, which is hosted by the European Bioinformatics Institute and the Centre for Genomic Regulation under accession no. EGAS00001005507. Gene expression matrices and the Seurat object (which contains associated cell metadata for all cells that passed upstream QC and is the input file for most scripts) have been deposited on the Oxford Research Archive and can be found with DOI XXX   


### Contact ###

For questions or queries please contact benjmain.fairfax@oncology.ox.ac.uk